import pygame
import vector
class spatialhash(object):
	def __init__(self,x,y,s):
		self.size = s
		self.xlen = x/s
		self.ylen = y/s
		self.hash = []
		for x in range(0,self.xlen):
			self.hash.append([])
			for y in range (0,self.ylen):
				self.hash[x].append([])
	def debug(self):
		size = self.size
		from main import surf_bgr
		for x in range(0,len(self.hash)):
			for y in range(0,len(self.hash[x])):
				if len(self.hash[x][y]) == 0:
					pygame.draw.rect(surf_bgr, (230,230,230),(x*size, y*size ,size ,size),1)
				else:
					pygame.draw.rect(surf_bgr, (230,130,130),(x*size, y*size ,size ,size),1)
					


						
def getdircost(loc1,loc2):    
	if loc1[0] - loc2[0] != 0 and loc1[1] - loc2[1] != 0:
		return 1.4 # diagnal movement
	else:
		return 1.0 # horizontal/vertical movement 
		
def get_h_score(end,current):
	hscore = (abs(end[0]-current[0])+abs(end[1]-current[1])) * 10
	return hscore
	
def closest_enemy(a,hash, searcher): #find closest enemy
	from main import size
	#print "from", a , "to", b
	openset = [] # The set of tentative nodes to be evaluated, initially containing the start node
	closedset = []
	start = [int(a[0])/size,int(a[1])/size] #pos
	# Cost from start along best known path.		
	openset.append(start)
	current = start
	done = False
	while not done:
		closedset.append(current)
		openset.remove(current)
		for os_x,os_y in [[-1,0],[1,0],[0,1],[0,-1]]:
			test = [current[0]+os_x , current[1]+os_y]
			if 0<test[0]<hash.xlen and 0<test[1]<hash.ylen:
				for obj in hash.hash[test[0]][test[1]]:
					if "CHAR" in obj.FLAGS and not obj == searcher:
						obj.colour = [255,0,50]
						return obj
				cond = True
				if not (test in closedset or test in openset):
					openset.append(test)


		current = openset[0]

			
def Astar( a, b, hash): #A* algorithm
	from main import size
	a= [int(a[0])/size,int(a[1])/size]
	b= [int(b[0])/size,int(b[1])/size]
	#print "from", a , "to", b

	openset = [] # The set of tentative nodes to be evaluated, initially containing the start node
	closedset = []
	start = [ [a[0], a[1]] , None , 0 , get_h_score(b,a), get_h_score(b,a)] #pos , parent , g score, h score, f score
	# Cost from start along best known path.		
	openset.append(start)
	current = start
	done = False
	while not done:
		closedset.append(current)
		if current[0] == b:
			done = True
			return current
		try:
			openset.remove(current)
		except ValueError:
			from main import PLAYER
			print PLAYER.pos
			print "creating path from",a,"to",b,". Error at", current
			return
		for os_x,os_y in [[-1,0],[1,0],[0,1],[0,-1]]:
			test = [current[0][0]+os_x , current[0][1]+os_y]
			if 0<test[0]<hash.xlen and 0<test[1]<hash.ylen:
				cond = True
				if not test == b:
					for obj in hash.hash[test[0]][test[1]]:
						if "STAT" in obj.FLAGS or "ENV" in obj.FLAGS:
							cond = False
				if cond:
					gscore = getdircost(test,current[0]) + current[2]
					hscore = get_h_score(b, test)
					next = [ [test[0], test[1]] , current , gscore , hscore , gscore+hscore] #pos , parent , g score, h score, f score
					cond = True
					for n in closedset:
						if n[0] == test:
							cond = False
					if cond:
						for n in openset:
							if n[0] == test:
								cond = False
						if cond:
							openset.append(next)
						else:
							if next[2] >= getdircost(test,current[0]) + current[2]:
								next[1] = current
		if len(openset) == 0:
			print "no path"
		try:
			current = min (openset,key=lambda node:node[4])
		except ValueError:
			print openset
		
		
def draw_path(tile): #devbyus
	from main import hash
	done = False
	while not done:
		if tile[1] == None:
			done = True
		else:
			hash.hash[tile[0][0]][tile[0][1]].append("Line")
			tile = tile[1]
	
def find_waypoints( a, b, hash): #debygs
	tile = Astar( a, b, hash)
	from main import hash, surf_bgr
	done = False
	start = tile[0]
	tile = tile[1]

	while not done:
		if tile[1] == None:
			pygame.draw.line(surf_bgr, (255,0,0), [start[0]*hash.size+hash.size/2,start[1]*hash.size+hash.size/2], [tile[0][0]*hash.size+hash.size/2,tile[0][1]*hash.size+hash.size/2], 1) 
			done = True
		elif not vector.line_shash([start[0]*hash.size+hash.size/2,start[1]*hash.size+hash.size/2],[tile[1][0][0]*hash.size+hash.size/2,tile[1][0][1]*hash.size+hash.size/2]):
			pygame.draw.line(surf_bgr, (255,0,0), [start[0]*hash.size+hash.size/2,start[1]*hash.size+hash.size/2] ,[tile[1][0][0]*hash.size+hash.size/2,tile[1][0][1]*hash.size+hash.size/2], 1) 
			start = tile[1][0]
		else:
			tile = tile[1]
			
def get_waypoint( a, b, hash): 
	tile = Astar( a, b, hash)
	if not tile == None:
		from main import hash
		done = False
		start = tile[0]
		if tile[1] == None: #in case on one tile long hashes
			return [tile[0][0]*hash.size+hash.size/2,tile[0][1]*hash.size+hash.size/2]
			done = True
		tile = tile[1]
		while not done:
			if tile[1] == None:
				return [tile[0][0]*hash.size+hash.size/2,tile[0][1]*hash.size+hash.size/2]
				done = True
			elif not vector.line_shash([start[0]*hash.size+hash.size/2,start[1]*hash.size+hash.size/2],[tile[1][0][0]*hash.size+hash.size/2,tile[1][0][1]*hash.size+hash.size/2]):
				#return [tile[1][0][0]*hash.size+hash.size/2,tile[1][0][1]*hash.size+hash.size/2]
				return [tile[0][0]*hash.size+hash.size/2,tile[0][1]*hash.size+hash.size/2]
			else:
				tile = tile[1]