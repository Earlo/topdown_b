import pygame
import vector
import math
class fist (object):
	def __init__(self,user):
		self.user = user
		self.fists = [punch(user,-1),punch(user,1)]
		self.aturn = 0 #mode of the weapon in use, will be made smarter later
	def charge(self):
		if (self.fists[self.aturn].charge<self.fists[self.aturn].maxcharge):
			self.fists[self.aturn].charge +=1
			self.fists[self.aturn].colour = [2*self.fists[self.aturn].charge,200-2*self.fists[self.aturn].charge,0]
			self.fists[self.aturn].rad = 3 + 1*(self.fists[self.aturn].charge/50)

	def shoot(self):
		if 	(self.fists[self.aturn].state == 0 and not self.fists[self.aturn-1].state == 2):
			self.user.stamina-=10
			self.fists[self.aturn].state = 2
			self.fists[self.aturn].time = 4 + (self.fists[self.aturn].charge/50)
			self.fists[self.aturn].spd = 5 + (self.fists[self.aturn].charge/20)
			self.fists[self.aturn].rect.w = self.fists[self.aturn].rad*2
			self.fists[self.aturn].rect.h = self.fists[self.aturn].rad*2
			
			self.aturn += 1
			if self.aturn > len(self.fists)-1:
				self.aturn = 0

	def draw(self):
		for fist in self.fists:
			fist.step()

class punch (object):
	def __init__(self,user,side):
		self.side = side
		self.SHAPE = "CIRC"
		self.FLAGS = ["MOBI","ATCK"]
		self.dis = 0
		self.rad = 3
		self.spd = 5
		self.time = 0
		self.state = 0 # 0 = inactive; 1 = returning, 2 = attacking
		self.charge = 0
		self.maxcharge = 100
		self.colour = [2*self.charge,200-2*self.charge,0]
		self.user = user
		pos = vector.vpointnormal( self.user.pos, self.user.looking_vec, self.user.rad/2*self.side )
		self.offset = [self.user.pos[0]-pos[0],self.user.pos[1]-pos[1]]
		self.uv = self.user.looking_vec
		self.rect = pygame.Rect(-self.rad*2,-self.rad*2, self.rad*2,self.rad*2)
		self.rect.center = [int(self.user.pos[0]+self.offset[0]),int(self.user.pos[1]+self.offset[1])]
		self.hashes = []
		#self.hashpend()

	def step(self):
		from main import updates,erase,  MainWindow, hash,surf_obj, camera

		u_rect = self.rect.copy()
		u_rect.topleft = [self.rect.topleft[0]-camera.topleft[0],self.rect.topleft[1]-camera.topleft[1]]
		updates.append(u_rect)		
		
		self.uv = self.user.looking_vec
		
		if self.state == 2: #punching out
			self.offset = (self.offset[0]+self.uv[0]*self.spd ,self.offset[1]+self.uv[1]*self.spd)
			self.rect.center = [int(self.user.pos[0]+self.offset[0]),int(self.user.pos[1]+self.offset[1])]
			self.collision()
			self.hashmove()
			self.time -= 1
			if self.time < 1:
				tar = vector.vpointnormal( self.user.pos, self.user.looking_vec, -self.user.rad/2*self.side )
				self.dis = vector.simple_distance(self.rect.center, tar )
				self.state = 1
				
		elif self.state == 1: #cooling down
			self.dis = self.dis - self.spd
			tar = vector.vpointnormal( self.user.pos, self.user.looking_vec, -self.user.rad/2*self.side )
			if self.dis > 0:
				self.rect.center = vector.vpoint(tar,self.uv,self.dis)
			else:
				self.dis = 0
				self.rect.center = tar
				self.state = 0
				self.charge = 0
				self.colour = [0,200,0]
				self.rad = 3

		else:
			pos = vector.vpointnormal( self.user.pos, self.user.looking_vec, self.user.rad/2*self.side )
			self.offset = [self.user.pos[0]-pos[0],self.user.pos[1]-pos[1]]
			self.rect.center = [int(self.user.pos[0]+self.offset[0]),int(self.user.pos[1]+self.offset[1])]
		#print self.rect.center
		pygame.draw.circle(surf_obj, self.colour, self.rect.center, self.rad, 0)
		#MainWindow.blit(surf_obj,(0,0))
		MainWindow.blit(surf_obj,self.rect.topleft,self.rect)
		u_rect = self.rect.copy()
		u_rect.topleft = [self.rect.topleft[0]-camera.topleft[0],self.rect.topleft[1]-camera.topleft[1]]
		updates.append(u_rect)
		erase.append(self.rect)

		
	def collision(self):
		self.hashpend()
		for hash in self.hashes:
			if len(hash) > 1:
				for obj in hash:
					if not (obj == self or obj == "Line" ):
						if obj.SHAPE == "CIRC":
							if "CHAR" in obj.FLAGS:
								if not obj == self.user:
									dis = vector.simple_distance(self.rect.center,obj.rect.center)
									if (dis < self.rad+obj.rad):
										self.hit(obj)
										return 0
						elif obj.rect.colliderect(self.rect):
							self.hit(obj)
							return 0
	def hit(self,target):
		self.state = 1
		if "CHAR" in target.FLAGS:
			target.momentum[0] = [self.uv[0]*(4+(self.charge**1.8)/50) , self.uv[1]*(4+(self.charge**1.8)/50)]
			target.momentum[1] = int(self.charge**2/(1+self.charge**1.6))
			#target.momentum[0] = [self.uv[0]*(3+(self.charge**1.1)) , self.uv[1]*(3+(self.charge**1.1))]
	def hashpend(self):
		from main import hash
		start = [self.rect.topleft[0]/hash.size,	self.rect.topleft[1]/hash.size]
		xdif = self.rect.right/hash.size -self.rect.left/hash.size
		ydif = self.rect.bottom/hash.size -self.rect.top/hash.size 
		for x in range(0,xdif+1):
			for y in range(0,ydif+1):
				hash.hash[start[0]+x][start[1]+y].append(self)
				self.hashes.append(hash.hash[start[0]+x][start[1]+y])
	def hashmove(self):
		for hash in self.hashes:
			hash.remove(self)
		self.hashes=[]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class bat (object):
	def __init__(self,user):
		self.user = user
		self.attack = swing(user,-1)
	def charge(self):
		if (self.attack.charge<self.attack.maxcharge):
			self.attack.charge +=1
			self.attack.colour = [2*self.attack.charge,200-2*self.attack.charge,0]

	def shoot(self):
		if 	self.attack.state == 0:
			self.user.stamina-=10
			self.attack.state = 1
			self.attack.time = 4 + (self.attack.charge/50)
			self.attack.spd = 15 + (self.attack.charge/10)
			
	def draw(self):
		self.attack.step()
		
class swing (object):
	def __init__(self,user,side):
		self.side = side
		self.SHAPE = "LINE"
		self.FLAGS = ["MOBI","ATCK"]
		self.len = 25
		self.charge = 0
		self.max_angle = 60 + self.charge/5
		self.angle =  0 #self.max_angle*side
		self.spd = 5
		self.state = 0 # 0 = inactive; 1 = returning, 2 = attacking
		self.maxcharge = 100
		self.colour = [2*self.charge,200-2*self.charge,0]
		self.user = user
		self.width = 3
		#pos = vector.vpointnormal( self.user.pos, self.user.looking_vec, self.user.rad/2*self.side )
		#self.offset = [self.user.pos[0]-pos[0],self.user.pos[1]-pos[1]]
		#self.uv = self.user.looking_vec
		uvec = vector.uvturn(self.user.dir+self.angle)
		self.end = vector.vpoint(self.user.rect.center,uvec,self.len)
		rend = [-(self.user.rect.center[0]-self.end[0]),-(self.user.rect.center[1]-self.end[1])]
		self.rect = pygame.Rect(self.user.rect.center,rend)
		self.rect.normalize()
		self.rect=self.rect.inflate(self.width+5,self.width+5) 
		#self.rect.center = [int(self.user.pos[0]+self.offset[0]),int(self.user.pos[1]+self.offset[1])]
		self.hashes = []

	def step(self):
		from main import updates,erase,  MainWindow, hash,surf_obj, camera

		u_rect = self.rect.copy()
		u_rect.topleft = [self.rect.topleft[0]-camera.topleft[0],self.rect.topleft[1]-camera.topleft[1]]
		updates.append(u_rect)				
				
		if self.state == 1: #Swinging
			self.angle -= self.spd*self.side
			uvec = vector.uvturn(self.user.dir+self.angle)
			self.end = vector.vpoint(self.user.rect.center,uvec,self.len)
			rend = [-(self.user.rect.center[0]-self.end[0]),-(self.user.rect.center[1]-self.end[1])]

			self.collision()
			if math.fabs(self.angle) > self.max_angle:
				if self.angle > self.max_angle:
					self.angle = 60
				else:
					self.angle = -60
				self.state = 0
				self.side = self.side*-1					
				self.charge = 0


		else: #standby
			self.max_angle = 60 + self.charge/5
			if math.fabs(self.angle) + self.spd > self.max_angle:
				self.angle = self.max_angle*self.side
			else:
				self.angle = self.angle+self.spd*self.side
			self.colour = [2*self.charge,200-2*self.charge,0]
			uvec = vector.uvturn(self.user.dir+self.angle)
			
			self.end = vector.vpoint(self.user.rect.center,uvec,self.len)
			rend = [-(self.user.rect.center[0]-self.end[0]),-(self.user.rect.center[1]-self.end[1])]
		
		self.rect = pygame.Rect(self.user.rect.center,rend)

		self.rect.normalize()
		self.rect=self.rect.inflate(self.width+5,self.width+5) 
		pygame.draw.line(surf_obj, self.colour, self.user.rect.center, self.end, self.width)
		
		#MainWindow.blit(surf_obj,self.rect.topleft,self.rect)
		MainWindow.blit(surf_obj,self.user.rect.center,self.rect)
		u_rect = self.rect.copy()
		u_rect.topleft = [self.rect.topleft[0]-camera.topleft[0],self.rect.topleft[1]-camera.topleft[1]]
		
		#pygame.draw.rect(surf_obj, self.colour, self.rect, self.width)
		#pygame.draw.rect(surf_obj, (255,255,0), u_rect, self.width)

		updates.append(u_rect)
		erase.append(self.rect)

		
	def collision(self):
		self.hashmove()
		self.hashpend()
		for hash in self.hashes:
			if len(hash) > 1:
				for obj in hash:
					if not (obj == self or obj == self.user):
						if obj.SHAPE == "CIRC":
							if "CHAR" in obj.FLAGS:
								if vector.seg_circ(self.user.rect.center, self.end, obj.pos, obj.rad, self.width) < obj.rad*2:
									self.hit(obj)
									return 0
						elif vector.seg_rect((self.user.rect.center,self.end),obj.rect):
							self.hit(obj)
							return 0
	def hit(self,target):
		#print target.FLAGS
		#print "moi"
		if "CHAR" in target.FLAGS:
			dir = vector.vvectornormal(vector.uvector(self.user.rect.center,self.end),1,self.side)
			target.momentum[0] = [dir[0]*(4+(self.charge**1.8)/50) , dir[1]*(4+(self.charge**1.8)/50)]
			target.momentum[1] = int(self.charge**2/(1+self.charge**1.6))
			#target.momentum[0] = [self.uv[0]*(3+(self.charge**1.1)) , self.uv[1]*(3+(self.charge**1.1))]
		if self.charge < 50:
			self.state = 0
			if self.angle<0:
				self.side = -1
			else:
				self.side = 1
			self.charge = 0
			self.speed = 10
		else:
			self.charge -= 20 
			
	def hashpend(self):
		#print "from",s,"to",e
		s = self.user.rect.center
		e = self.end
		from main import hash
		size = hash.size
		s_h = [int(s[0])/size,int(s[1])/size]
		e_h = [int(e[0])/size,int(e[1])/size]
		self.hashes = []
		issteep = abs(e_h[1]-s_h[1]) > abs(e_h[0]-s_h[0])
		if issteep:
			s_h[0], s_h[1] = s_h[1], s_h[0]
			e_h[0], e_h[1] = e_h[1], e_h[0]
		rev = False
		if s_h[0] > e_h[0]:
			s_h[0], e_h[0] = e_h[0], s_h[0]
			s_h[1], e_h[1] = e_h[1], s_h[1]
			rev = True
		deltax = e_h[0] - s_h[0]
		deltay = abs(e_h[1]-s_h[1])
		error = int(deltax / 2)
		y = s_h[1]
		ystep = None
		if s_h[1] < e_h[1]:
			ystep = 1
		else:
			ystep = -1
		for x in range(s_h[0], e_h[0] + 1):
			if issteep:
				self.hashes.append(hash.hash[y][x])
				hash.hash[y][x].append(self)
			else:
				self.hashes.append(hash.hash[x][y])
				hash.hash[x][y].append(self)
			error -= deltay
			if error < 0:
				y += ystep
				error += deltax
		# Reverse the list if the coordinates were reversed

	#	for point in self.hashes:
	#		if not len(hash.hash[point[0]][point[1]]) == 0:
	#			for obj in hash.hash[point[0]][point[1]]:
	#				if "ENV" in obj.FLAGS:
	#					obj.debug_draw()
	#					if seg_rect([s,e],obj.rect):
	#						return False #something is in the way
							
	def hashmove(self):
		for hash in self.hashes:
			hash.remove(self)
		self.hashes=[]