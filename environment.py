import pygame
class wall(object):
	def __init__(self,rect):
		self.rect = pygame.Rect(rect)
		self.colour = (0,0,0)
		self.momentum = [[0,0],False]
		self.hashes = []
		self.hashpend()
		self.SHAPE = "RECT"
		self.FLAGS = ["STAT","ENV"]
	def draw(self):
		
		from main import updates, MainWindow,surf_bgr,camera
		u_rect = self.rect.copy()
		#u_rect = self.rect.copy().move(map(sum, zip(self.rect.topleft, camera.topleft)))

		pygame.draw.rect(surf_bgr, self.colour, u_rect, 0)

		MainWindow.blit(surf_bgr,u_rect.topleft,u_rect)
		updates.append(u_rect)
		
		
	def debug_draw(self):
		from main import updates, MainWindow,surf_obj
		r_lines = [[self.rect.bottomleft,self.rect.topright],[self.rect.bottomright,self.rect.topleft]]
		for line in r_lines:
			pygame.draw.line(surf_obj, (255,25,2), line[0],line[1], 1)
		MainWindow.blit(surf_obj,self.rect.topleft,self.rect)
		updates.append(self.rect)
	def hashpend(self):
		from main import hash
		x_s = [self.rect.left/hash.size,(self.rect.right-1)/hash.size]
		y_s = [self.rect.top/hash.size ,(self.rect.bottom-1)/hash.size]
		for x in range(x_s[0],x_s[1]+1):
			for y in range(y_s[0],y_s[1]+1):
				hash.hash[x][y].append(self)
				self.hashes.append(hash.hash[x][y])
	def hit(self,force,dir):
		print "wall was hit"