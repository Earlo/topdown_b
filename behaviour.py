import vector
import pygame
import spatialh
from pygame.locals import *

def player(char):
	from main import keys, mouse, camera
	offset = [0,0,[0,0]] 
	
	if keys[K_SPACE]:
		if char.enemy == None:
			from main import hash
			char.enemy = spatialh.closest_enemy(char.pos,hash,char)
			char.lockon = True			
	elif not keys[K_SPACE] and not char.enemy == None:
		char.enemy.colour = [200,100,0]
		char.enemy = None
		
	if char.enemy == None: #free movement
		m_pos = map(sum, zip(pygame.mouse.get_pos(), camera.topleft)) 
		char.looking_dir = vector.vdir(vector.uvector(char.pos,m_pos))
		char.looking_vec = vector.uvturn(char.looking_dir)
		if vector.vangle(char.looking_vec,char.uv) < 0.85:
			if mouse[0] == 1: 
				if vector.vangle(char.looking_vec,vector.uvturn(char.dir-10)) < vector.vangle(char.looking_vec,vector.uvturn(char.dir+10)):
					offset[0] = 1
				else:
					offset[0] = -1
			if vector.vangle(char.looking_vec,char.uv) < 0.4:
				if vector.vangle(char.looking_vec,vector.uvturn(char.dir-60)) < vector.vangle(char.looking_vec,vector.uvturn(char.dir+60)):
					char.looking_vec = vector.uvturn(char.dir+60)

				else:
					char.looking_vec = vector.uvturn(char.dir-60)

		
		if keys[K_a]:
			offset[0] = 1
		elif keys[K_d]:
			offset[0] = -1
		if keys[K_w]:
			offset[1] = 1
		elif keys[K_s]:
			offset[1] = -1

		char.move(offset)
		
		if mouse[0] == 1: 
			if char.stamina > 10 and char.charging == False:
				char.stamina-=10
				char.charging = True
				char.weapon.charge()
			elif char.charging == True:
				char.stamina -= 0.1
				char.weapon.charge()
		elif mouse[0] == 0 and char.charging == True:
			char.weapon.shoot()
			char.charging = False
			

	else: #locked on 
		char.looking_dir = vector.vdir(vector.uvector(char.pos,char.enemy.pos))
		char.looking_vec = vector.uvturn(char.looking_dir)
		char.dir =char.looking_dir 
		if not vector.vangle(char.looking_vec,char.uv) == 1:
			if vector.vangle(char.looking_vec,vector.uvturn(char.dir-1)) < vector.vangle(char.looking_vec,vector.uvturn(char.dir+1)):
				char.looking_vec = vector.uvturn(char.dir)
				offset[0] = 1
			else:
				char.looking_vec = vector.uvturn(char.dir)
				offset[0] = -1


		if keys[K_a]:
			offset[2] = [1,5]
		elif keys[K_d]:
			offset[2] = [-1,5]
		if keys[K_w]:
			offset[1] = 1
		elif keys[K_s]:
			offset[1] = -1

		char.move(offset)
				
		if mouse[0] == 1: 
			if char.stamina > 10 and char.charging == False:
				char.stamina-=10
				char.charging = True
				char.weapon.charge()
			elif char.charging == True:
				char.stamina -= 0.1
				char.weapon.charge()
		elif mouse[0] == 0 and char.charging == True:
			char.weapon.shoot()
			char.charging = False		

def stander(char):		
	offset = [0,0,[0,0]] 
	char.move(offset)
	
def offender(char):
	
	offset = [0,0,[0,0]] 

	#debugs
	from main import size,hash
	if vector.line_shash(char.pos, char.enemy.pos): #can target be seen?
		char.waypoint = None
		dis = vector.simple_distance(char.pos,char.enemy.pos)
		char.looking_dir = vector.vdir(vector.uvector(char.pos,char.enemy.pos))
		char.looking_vec = vector.uvturn(char.looking_dir)
		if dis > 30:
			offset[1] = 1	
		if vector.vangle(char.looking_vec,char.uv) < 0.85:
			if vector.vangle(char.looking_vec,vector.uvturn(char.dir-10)) < vector.vangle(char.looking_vec,vector.uvturn(char.dir+10)):
				offset[0] = 1
			else:
				offset[0] = -1		
			if vector.vangle(char.looking_vec,char.uv) < 0.4:
				offset[1] = 0
				if vector.vangle(char.looking_vec,vector.uvturn(char.dir-60)) < vector.vangle(char.looking_vec,vector.uvturn(char.dir+60)):
					char.looking_vec = vector.uvturn(char.dir+60)
				else:
					char.looking_vec = vector.uvturn(char.dir-60)
					
		if (dis < 50 and (dis > 30 or char.charging == False)) or char.stamina < 50: 
			if char.stamina > 10 and char.charging == False:
				char.charging = True
				char.weapon.charge()
			elif char.charging == True:
				char.stamina -= 0.1
				char.weapon.charge()
		elif dis < 30 and char.charging == True:
			char.weapon.shoot()
			char.charging = False
			
	elif char.waypoint == None:
		char.waypoint = spatialh.get_waypoint(char.enemy.rect.center,char.rect.center,hash)
		#print char.waypoint
		
	else:
		if not vector.line_shash(char.pos, char.waypoint): #can waypoint be seen?
			char.waypoint = spatialh.get_waypoint(char.enemy.rect.center,char.rect.center,hash)
		dis = vector.simple_distance(char.pos,char.waypoint)
		char.looking_dir = vector.vdir(vector.uvector(char.pos,char.waypoint))
		char.looking_vec = vector.uvturn(char.looking_dir)
		if dis < 50:
			char.waypoint = spatialh.get_waypoint(char.enemy.rect.center,char.rect.center,hash)
			#print char.waypoint

		if dis > 15:
			offset[1] = 1

			
		if vector.vangle(char.looking_vec,char.uv) < 0.85:
			if vector.vangle(char.looking_vec,vector.uvturn(char.dir-10)) < vector.vangle(char.looking_vec,vector.uvturn(char.dir+10)):
				offset[0] = 1
			else:
				offset[0] = -1		
			if vector.vangle(char.looking_vec,char.uv) < 0.4:
				offset[1] = 0
				if vector.vangle(char.looking_vec,vector.uvturn(char.dir-60)) < vector.vangle(char.looking_vec,vector.uvturn(char.dir+60)):
					char.looking_vec = vector.uvturn(char.dir+60)
				else:
					char.looking_vec = vector.uvturn(char.dir-60)
		
	char.move(offset)

"""	elif keys[K_DOWN]:
		offset[1] = -1
	char.move(offset)
	if mouse[0] == 1: 
		if char.stamina > 10 and char.charging == False:
			char.stamina-=10
			char.charging = True
			char.weapon.charge()
		elif char.charging == True:
			char.stamina -= 0.1
			char.weapon.charge()
	elif mouse[0] == 0 and char.charging == True:
		char.weapon.shoot()
		char.charging = False"""