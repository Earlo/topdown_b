# coding: utf-8
#This is the main file. Run this, to run the program
import random
import pygame
from pygame.locals import *
import math
import sys

import environment
import player
import spatialh
import screen

import vector

import behaviour
#Setup
pygame.init()
pygame.font.init()
#colors
black 	= (0	,0		,0)
red		= (255	,0		,0)
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)
# set up fonts
basicFont = pygame.font.SysFont(None, 48)

FPS = 60 # 60 frames per second
clock = pygame.time.Clock()

done = False


#window and setting up display stuff
SWIDTH =  1000
SHEIGTH = 600
global MainWindow, camera
MainWindow = pygame.display.set_mode((SWIDTH, SHEIGTH))
surf_obj = pygame.Surface((2000,1200) )
surf_bgr = pygame.Surface((2000,1200) )
surf_bgr.fill(white)
surf_obj.fill(white)
surf_obj.set_colorkey(white)
LevelRect = pygame.Rect(0,0,2000,1200)
camera = screen.Camera(LevelRect, SWIDTH, SHEIGTH)
#setting up spatial hash
size = 25
hash = spatialh.spatialhash(SWIDTH*2, SHEIGTH*2,size)
MainWindow.blit(surf_bgr,(0,0))
#pygame.display.flip()

updates = []
erase = []
draw = []
PLAYER = player.player([200,200],(0,100,200),behaviour.player)
NPC = []

for x in range(0,2):
	NPC.append(player.player([300+20*x,550],(200,100,0),behaviour.offender))
	NPC[x].spd = 2


for x in range(0,2):
	NPC[x].enemy =	NPC[x-1]
import projectile
NPC[0].weapon = projectile.bat(NPC[0])	
PLAYER.weapon = projectile.bat(PLAYER)
#PLAYER.spd = 100
	
#dude1 = player.player([300,300],(200,100,0),behaviour.offender)
#dude2 = player.player([500,300],(200,100,0),behaviour.offender)
#dude3 = player.player([600,300],(200,100,100),behaviour.offender)
#dude3 = player.player([600,200],(200,100,0),behaviour.stander)
#dude1.enemy = dude2
#dude2.enemy = dude1
#NPC.append(dude1)
#NPC.append(dude2)
#dude3.enemy = PLAYER
#dude3.spd = 2.5
#PLAYER.spd = 2.5
#PLAYER.enemy = dude3
#NPC.append(dude3)
#NPC.append(dude4)


	
W = environment.wall
walls = [W((0,0,SWIDTH*2,20)),W((0,0,20,SHEIGTH*2)),W((SWIDTH-20,0,20,SHEIGTH)),W((0,SHEIGTH-20,SWIDTH-100,20)),W((SWIDTH*2-20,0,20,SHEIGTH*2)),W((0,SHEIGTH*2-20,SWIDTH*2,20))]
walls.append(W((400,100,20,400)))
#walls.append(W((100,200,400,20)))

for wall in walls:
	wall.draw()
	
#hash.debug()

pygame.display.update(updates)
updates = []


#debug 2
start = None
end = None
MainWindow.blit(surf_bgr,(0,0))
pygame.display.flip()
while not done:
	mouse = pygame.mouse.get_pressed()
				
	for event in pygame.event.get(): # User did something
		if event.type == pygame.QUIT: # Closed from X
			done = True # Stop the Loop
	keys = pygame.key.get_pressed()
	for place in erase:
		surf_obj.blit(surf_bgr,place.topleft,place)
		MainWindow.blit(surf_bgr,place.topleft,place)
	erase = []
	updates = []
	#debug 3
	#hash.debug()
	MainWindow.blit(surf_bgr,camera.topleft,camera)
	#pygame.display.flip()
	
	#for wall in walls:
	#	wall.draw()#for now drawn in here, eventually anything that is seen will be first calculated in players step funk and after that drawn
	for npc in NPC:
		npc.step()
	PLAYER.step()
	#print PLAYER.pos
	#camera.center = PLAYER.pos
	f = camera.update(PLAYER.rect,LevelRect)
	#MainWindow.blit(surf_obj,(0,0)) #Dims out places that can't be seen
	MainWindow.blit(surf_bgr,(0,0),camera) 
	MainWindow.blit(surf_obj,(0,0),camera) 
	if not f:
		pygame.display.update(updates)
	else:
		pygame.display.flip()

	#pygame.display.flip()
	updates = []

#	surf_dar.fill((50,50,50)) 
	clock.tick(FPS)
	pygame.display.set_caption("FPS: %i" % clock.get_fps())
	
sys.exit(0)