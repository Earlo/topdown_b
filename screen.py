import pygame

global map_size
black 	= (0	,0		,0)
red		= (255	,0		,0)
lgreen	= (0	,255	,0)
green	= (0	,155	,0)
dgreen	= (0	,55		,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)
pygame.font.init()

class screen (object):		#this is base surface of terrain
	def __init__(self, RWIDTH, RHEIGTH):
		from main import SWIDTH , SHEIGTH


		
class Camera(pygame.Rect):
    cameraSlackX = 80
    cameraSlackY = 60
    def __init__(self, targetRect, windowWidth, windowHeight):
        super(Camera,self).__init__(targetRect.centerx-(windowWidth/2),
                                    targetRect.centery-(windowHeight/2),
                                    windowWidth, windowHeight)
        
    def update(self, rect, level):
        self.pre_pos = self.topleft
        #self.centerx = rect.centerx
        #self.centery = rect.centery
        # Figure out if rect has exceeded camera slack
        if self.centerx - rect.centerx > self.cameraSlackX:
            self.left = rect.centerx + self.cameraSlackX - self.width/2
        elif rect.centerx - self.centerx > self.cameraSlackX:
            self.left = rect.centerx - self.cameraSlackX - self.width/2
        if self.centery - rect.centery > self.cameraSlackY:
            self.top = rect.centery + self.cameraSlackY - self.height/2
        elif rect.centery - self.centery > self.cameraSlackY:
            self.top = rect.centery - self.cameraSlackY - self.height/2
        
        # This keeps the camera within the boundaries of the level
        if self.right > level.right:
            self.right = level.right
        elif self.left < level.left:
            self.left = level.left
        if self.top < level.top:
            self.top = level.top
        elif self.bottom > level.bottom:
            self.bottom = level.bottom
        #self.topleft = [-self.topleft[0],-self.topleft[1]]
        if not self.topleft == self.pre_pos:
            from main import MainWindow, surf_bgr
            MainWindow.blit(surf_bgr,self.topleft,self) 
            return True
        else:
            return False
