import pygame
import vector
import projectile
import math
import random


class player (object):
	def __init__(self,pos,colour,behaviour):
		self.pos = pos
		self.dir = 180
		self.rad = 10
		self.SHAPE = "CIRC"
		self.rect = pygame.Rect(-self.rad*2,-self.rad*2, self.rad*2,self.rad*2)
		self.rect.center = self.pos
		self.FLAGS = ["MOBI","PLAYER","CHAR"]
		self.spd = 3
		self.fow = 120
		self.uv = vector.uvturn(self.dir)
		self.looking_dir = vector.vdir(vector.uvector(self.pos,pygame.mouse.get_pos()))
		self.looking_vec = vector.uvturn(self.looking_dir)
		self.default_colour = colour
		self.colour = colour
		self.weapon = projectile.fist(self)
		self.hashes = []
		self.pos_change = [0,0]
		self.hashpend()
		self.stamina = 100 

		self.charging = False
		self.lockon = False
		self.enemy = None
		self.momentum = [[0,0],0] 
		self.moving = False
		self.waypoint = None
		self.behaviour = behaviour
		
	def collision(self,points):
		for point in points:
			#print point
			self.pos = [self.pos[0]+point[0],self.pos[1]+point[1]]
			self.rect.center = [int(self.pos[0]),int(self.pos[1])]
			self.hashpend()
			for hash in self.hashes:
				if len(hash) > 1:
					for obj in hash:
						if not (obj == self):
							if obj.SHAPE == "CIRC":
								if "CHAR" in obj.FLAGS:
									dis = vector.simple_distance(self.rect.center,obj.rect.center)
									if (dis < self.rad+obj.rad):
										vector.circ_circ(self,obj,dis)
							elif obj.SHAPE == "RECT": 
								if obj.rect.colliderect(self.rect):
									vector.rect_rect(obj,self)
									self.rect.center = [int(self.pos[0]),int(self.pos[1])]
			self.hashmove()
	def move(self,os):
		
		from main import updates,erase,  MainWindow, hash,surf_obj, camera

		u_rect = self.rect.copy()
		u_rect.topleft = [self.rect.topleft[0]-camera.topleft[0],self.rect.topleft[1]-camera.topleft[1]]
		updates.append(u_rect)	
		
		if not os[0] == 0:
			if not os[1] == 0:
				self.dir += os[0]*self.spd
			else:
				self.dir += os[0]*self.spd*3
			if self.dir <= -180 or self.dir >= 180:
				self.dir = self.dir - 360
			self.uv = vector.uvturn(self.dir)
			
		size = hash.size	
		if os[2] == [0,0]: #if not strafing 
			self.pos_change = [self.uv[0]*self.spd*os[1]+self.momentum[0][0] ,self.uv[1]*self.spd*os[1]+self.momentum[0][1]]
			#print "we need", int(vector.vlen(self.pos_change)/(self.rad/2)), "point tests"
			max =  int(vector.vlen(self.pos_change)/(self.rad/2))
			points = []
			if max == 0:
				points.append([self.pos_change[0] ,self.pos_change[1]])
			for x in range(0,max):
				points.append([self.pos_change[0]/max ,self.pos_change[1]/max])
			#points.append([self.pos_change[0] ,self.pos_change[1]])			
		
		else: #Strafing. kind of like really stupidly done at the moment, will think of something better later
			r = vector.simple_distance(self.pos,self.enemy.pos)
			P = self.spd/(r*math.pi*2)
			cp = vector.vdir(vector.vector(self.enemy.pos,self.pos))
			cp += (360*P)*os[2][0]
			vec = vector.uvturn(cp)
			npos = vector.vpoint(self.enemy.pos,vec,r)
			self.pos_change = map(sum, zip(vector.vector(self.pos,npos), self.momentum[0]))
			max =  int(vector.vlen(self.pos_change)/(self.rad/2))
			points = []
			if max == 0:
				points.append([self.pos_change[0] ,self.pos_change[1]])
			for x in range(0,max):
				points.append([self.pos_change[0]/max ,self.pos_change[1]/max])			

		if not self.momentum[0] == [0,0]:
				if self.momentum[1] == 0:
					self.colour = [100,50,0]
					self.momentum[0] = [self.momentum[0][0]*0.75,self.momentum[0][1]*0.75]	
					if self.momentum[0][0] + self.momentum[0][1] < 0.1:
						self.momentum[0] = [0,0]
						
				else:
					self.colour = [255,155,55]
					self.momentum[1] -= 1
					if self.momentum < 0:
						self.momentum[1] = 0
		else:
			self.colour = self.default_colour

		
		self.collision(points)
		self.rect.center = [int(self.pos[0]),int(self.pos[1])]
		self.hashpend()

		#THINGS ARE DRAWN HERE!
		#u_rect.move(map(sum, zip(A, B))
		#arc of vision stuff here
		lv = vector.uvturn(self.dir+60)
		rv = vector.uvturn(self.dir-60)
		#status bars 
		"""
		x = (19*self.weapon.fists[self.weapon.aturn].charge/100)
		pygame.draw.rect(MainWindow,(55,0,0),(self.pos[0]-10,self.pos[1]-13,20,-2),0)
		if (self.weapon.fists[self.weapon.aturn].charge > 0):
			pygame.draw.rect(MainWindow,(255,0,0),(self.pos[0]-10,self.pos[1]-13, x+1,-2),0)		
		x = (19*self.stamina/100)
		pygame.draw.rect(MainWindow,(0,0,55),(self.pos[0]-10,self.pos[1]-10,20,-2),0)
		pygame.draw.rect(MainWindow,(55,55,255),(self.pos[0]-10,self.pos[1]-10, x+1,-2),0)
		"""
		
		pygame.draw.circle(surf_obj, self.colour, self.rect.center, self.rad, 0)
		pygame.draw.line(surf_obj,(255,20,20,20),self.pos,vector.vpoint(self.pos,self.uv,self.rad-1),1)
		pygame.draw.line(surf_obj,(20,20,255,20),self.pos,vector.vpoint(self.pos,self.looking_vec,self.rad-1),1)

		self.weapon.draw()


		#MainWindow.blit(surf_obj,(0,0))
		#pygame.draw.polygon(surf_dar, (0,0,0,0), (self.rect.center,vector.vpoint(self.pos,lv,2000),vector.vpoint(self.pos,rv,2000)), 0)
		u_rect = self.rect.copy()
		u_rect.topleft = [self.rect.topleft[0]-camera.topleft[0],self.rect.topleft[1]-camera.topleft[1]]
		updates.append(u_rect)
		erase.append(self.rect)

		#MainWindow.blit(surf_obj,u_rect.topleft,self.rect)
		

	def marked(self):
		from main import camera
		p = self.rect.center
		l = []
		l.append(map(sum,zip(p,[0,-15])))
		l.append(map(sum,zip(p,[-5,-20])))
		l.append(map(sum,zip(p,[5,-20])))
		pygame.draw.polygon(camera.surf_bgr, (155,100,0), l ,0)
		
	def debug_waypoint(self):
		from main import surf_bgr
		pygame.draw.circle(surf_bgr, (50,100,200), self.waypoint, 10, 2)

	def step(self):		
		if self.stamina < 100:
			self.stamina +=.5

		self.behaviour(self)
		
	def hashpend(self):
		from main import hash
		start = [self.rect.topleft[0]/hash.size,	self.rect.topleft[1]/hash.size]
			
		xdif = self.rect.right/hash.size -self.rect.left/hash.size
		ydif = self.rect.bottom/hash.size -self.rect.top/hash.size 
		for x in range(0,xdif+1):
			for y in range(0,ydif+1):
				hash.hash[start[0]+x][start[1]+y].append(self)
				self.hashes.append(hash.hash[start[0]+x][start[1]+y])
	def hashmove(self):
		for hash in self.hashes:
			hash.remove(self)
		self.hashes=[]
		