import math


def vlen(vec): #returns the length of the vector
	return math.sqrt((vec[0])**2 + (vec[1])**2) 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def simple_distance(pos1,pos2): #returns distance between two positions
	return math.sqrt((pos1[0]-pos2[0])**2 + (pos1[1]-pos2[1])**2)
    
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def uvector(s,e): #start and end, returns unitvector pointing from start towards end.
	#print (s,e)
	vecx = (e[0] - s[0])
	vecy = (e[1] - s[1])
	length = (math.sqrt(vecx**2+vecy**2))
	unitvector = (vecx/length, vecy/length)
	return unitvector
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vector(s,e): #start and end, returns vector pointing from start to end.
	return [e[0] - s[0], e[1] - s[1]]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def uvectorl(s,e):  #Unit vector that makes sure there is no division by zero
	vecx = (s[0] - e[0])
	vecy = (s[1] - e[1])
	length = (math.sqrt(vecx**2+vecy**2))
	try:
		unitvector = (vecx/length, vecy/length,length)
	except ZeroDivisionError:
		unitvector = (-1,0)
	return unitvector
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def uvturn(dir): #returns a vector pointing in direction dir (degrees)
	dir = math.radians(dir)
	vec = [math.sin(dir),math.cos(dir)]
	return vec
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vdir(vec): #tell where the vector is pointing, opposite of function above
	return math.degrees(math.atan2(vec[0],vec[1]))
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	
def vpoint(pos,vec,len): #returns point at the end of vector vec of length len
	return [pos[0]+vec[0]*len,	pos[1]+vec[1]*len]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vpointnormal(pos,vec,len): #returns point at the end of vector vec's normal of length len
	return [pos[0]-vec[1]*len,	pos[1]+vec[0]*len]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vvectornormal(vec,len,s): #returns vec vectors normal vector of length len pointing at side s
	return [-vec[1]*len*s,vec[0]*len*s]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vangle (v1,v2):#returns angle between vec1 and vec2
	return (v1[0]*v2[0]+v1[1]*v2[1])/(math.sqrt(v1[0]**2+v1[1]**2)*math.sqrt(v1[0]**2+v1[1]**2))
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def ssift(pos,tar,dist): #vector movement from pos towards tar 
	vecx = (tar[0] - pos[0])
	vecy = (tar[1] - pos[1])
	v = (vecx*dist,vecy*dist)
	return v
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sift(pos,tar,len):
	vecx = (tar[0] - pos[0])
	vecy = (tar[1] - pos[1])
	length = math.fabs(math.sqrt(vecx**2+vecy**2))
	unitvector = ((vecx/length)*len, (vecy/length)*len)
	newx = int (round (pos[0] - unitvector[0]))
	newy = int (round (pos[1] - unitvector[1]))
	return (newx,newy)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#linesegment in spatialhash, rough test if anything is in the way of start and end.
def line_shash(s,e): #start, end and the size of the raster
	#print "from",s,"to",e
	from main import hash
	size = hash.size
	s_h = [int(s[0])/size,int(s[1])/size]
	e_h = [int(e[0])/size,int(e[1])/size]
	points = []
	issteep = abs(e_h[1]-s_h[1]) > abs(e_h[0]-s_h[0])
	if issteep:
		s_h[0], s_h[1] = s_h[1], s_h[0]
		e_h[0], e_h[1] = e_h[1], e_h[0]
	rev = False
	if s_h[0] > e_h[0]:
		s_h[0], e_h[0] = e_h[0], s_h[0]
		s_h[1], e_h[1] = e_h[1], s_h[1]
		rev = True
	deltax = e_h[0] - s_h[0]
	deltay = abs(e_h[1]-s_h[1])
	error = int(deltax / 2)
	y = s_h[1]
	ystep = None
	if s_h[1] < e_h[1]:
		ystep = 1
	else:
		ystep = -1
	for x in range(s_h[0], e_h[0] + 1):
		if issteep:
			points.append((y, x))
		else:
			points.append((x, y))
		error -= deltay
		if error < 0:
			y += ystep
			error += deltax
	# Reverse the list if the coordinates were reversed
	if rev:
		points.reverse()
	for point in points:
		if not len(hash.hash[point[0]][point[1]]) == 0:
			for obj in hash.hash[point[0]][point[1]]:
				if "ENV" in obj.FLAGS:
					obj.debug_draw()
					if seg_rect([s,e],obj.rect):
						return False #something is in the way
	return True  #nothing is in the way
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def ccw(A,B,C):
	return (C[1]-A[1])*(B[0]-A[0]) > (B[1]-A[1])*(C[0]-A[0])
	
def seg_intersect(A, B): #linesegment intersection	
	return ccw(A[0],B[0],B[1]) != ccw(A[1],B[0],B[1]) and ccw(A[0],A[1],B[0]) != ccw(A[0],A[1],B[1])
	
def seg_rect(seg,rect): #test if linesegment and rectangle intersect
	#r_lines = [[rect.bottomleft,rect.bottomright],[rect.bottomright,rect.topright],[rect.topright,rect.topleft],[rect.topleft,rect.bottomleft]]
	r_lines = [[rect.bottomleft,rect.topright],[rect.bottomright,rect.topleft]]
	for line in r_lines:
		if seg_intersect(seg, line):
			return True
	return False
	
#collision detections and stuff
def circ_circ(A,B,dis):
	overlap = (A.rad+B.rad)-dis
	dir= uvectorl(A.rect.center,B.rect.center)
	len= overlap/2
	if not B.momentum[0] == [0,0] or not A.momentum[0] == [0,0]:
		sum = [A.momentum[0][0]+B.momentum[0][0],A.momentum[0][1]+B.momentum[0][1]]
		A.momentum[0] = [sum[0]/2,sum[1]/2]
		B.momentum[0] = [sum[0]/2,sum[1]/2]
	if "STATIONARY" in A.FLAGS:
		B.pos = vpoint(B.rect.center,dir,-len*2)
	elif "STATIONARY" in B.FLAGS:
		A.pos = vpoint(A.rect.center,dir,len*2)
	else:
		B.pos = vpoint(B.rect.center,dir,-len*2)
		A.pos = vpoint(A.rect.center,dir,len*2)
	
def rect_rect(A,B):
	ar = math.atan2(A.rect.centery - A.rect.top, A.rect.right - A.rect.centerx) # half of the angle of the right side
	# this is normalized into [0, 2] to make searches easier (no negative numbers and stuff) 
	dirint = [ 2*ar, math.pi, math.pi+2*ar, 2*math.pi]
	# calculate angle towars the center of the other rectangle, + ar for normalization into
	ad = math.atan2(A.rect.centery - B.rect.centery, B.rect.centerx - A.rect.centerx) + ar
	# again normalization, sincen atan2 ouputs values in the range of [-pi,pi]
	if ad < 0:
		ad = 2*math.pi + ad
	# search for the quadrant we are in and return it
	for i in xrange(len(dirint)):
		if ad < dirint[i]:
			case = i
			break
	os_x = 0
	os_y = 0
	if case == 0: 	#B is on right side -> x axis
		d_x = B.rect.centerx-A.rect.centerx
		min_dx = (B.rect.width+A.rect.width)/2
		os_x = min_dx-d_x
		A.momentum[0][0] = -A.momentum[0][0]
		B.momentum[0][0] = -B.momentum[0][0]
	elif case == 1: # B is above -> y axis
		d_y = A.rect.centery-B.rect.centery
		min_dy = (B.rect.height+A.rect.height)/2
		os_y = -min_dy+d_y
		A.momentum[0][1] = -A.momentum[0][1]
		B.momentum[0][1] = -B.momentum[0][1]
	elif case == 2: # B is on left side -> x axis
		d_x = A.rect.centerx-B.rect.centerx
		min_dx = (B.rect.width+A.rect.width)/2
		os_x = -min_dx+d_x
		A.momentum[0][0] = -A.momentum[0][0]
		B.momentum[0][0] = -B.momentum[0][0]
	else: 			# B is below -> y axis
		d_y = B.rect.centery-A.rect.centery
		min_dy = (B.rect.height+A.rect.height)/2
		os_y = min_dy-d_y
		A.momentum[0][1] = -A.momentum[0][1]
		B.momentum[0][1] = -B.momentum[0][1]
	if "ENV" in A.FLAGS:
		B.pos = [B.pos[0] + os_x , B.pos[1] + os_y]
		return True
	elif "ENV" in B.FLAGS:
		A.pos = [A.pos[0] + os_x , A.pos[1] + os_y]
		return True
	else:
		B.pos = [B.pos[0] + os_x/2  ,B.pos[1] + os_y/2]
		A.pos = [A.pos[0] + os_x/2 , A.pos[1] + os_y/2]
		return True

	
#fuckingfuckfuck, i am just too dumb to get this work : P
"""def rect_circ(rect,p,rad):
	return rect.collidepoint(p) or
		line_circ((rect.topleft,rect.topright)p,rad) or
		line_circ((rect.topright,rect.bottomright)p,rad)) or
		line_circ((rect.bottomright,rect.bottomleft)p,rad) or
		line_circ((rect.bottomleft,rect.topleft)p,rad)"""
		
def closest_point_on_seg(seg_a, seg_b, circ_pos):
	seg_v = [seg_b[0]-seg_a[0],seg_b[1]-seg_a[1]]
	pt_v = [circ_pos[0]-seg_a[0],circ_pos[1]-seg_a[1]]
	#if seg_v.len() <= 0:
	#	raise ValueError, "Invalid segment length"
	seg_v_len = vlen(seg_v) 
	seg_v_unit = [seg_v[0] / seg_v_len , seg_v[1] / seg_v_len]
	proj = pt_v[0]*seg_v_unit[0]+pt_v[1]*seg_v_unit[1]
	if proj <= 0:
		return seg_a
	if proj >= vlen(seg_v):
		return seg_b
	proj_v = [seg_v_unit[0]*proj,seg_v_unit[1]*proj]
	closest = [proj_v[0] + seg_a[0],proj_v[1] + seg_a[1]]
	return closest

def seg_circ(seg_a, seg_b, circ_pos, circ_rad,width):
	closest = closest_point_on_seg(seg_a, seg_b, circ_pos)
	dist_v = [circ_pos[0]-closest[0],circ_pos[1]-closest[1]]
	print vlen(dist_v)
	#if dist_v.len() > circ_rad:
	#	return vec(0, 0)
	#if dist_v.len() <= 0:
	#	raise ValueError, "Circle's center is exactly on segment"
	#offset = dist_v / vlen(dist_v) * (circ_rad - vlen(dist_v))
	#print offset
    